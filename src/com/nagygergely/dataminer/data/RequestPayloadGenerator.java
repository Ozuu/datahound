package com.nagygergely.dataminer.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * This class generates a request playload in order to one can shuffle to the next page of the website.
 */

public class RequestPayloadGenerator {
    private final String basicPayloadFormFront = "xnxp=%3E%3E%3E&au=&au=";
    private final String basicPayLoadFormBack = "&xcom=1&xperpage=20&xdisp=4&tllang=&_charset_=UTF-8";

    public String generatePayload(String firstElementOfPreviousSite) throws UnsupportedEncodingException {
        String formattedElement = createProperElementForm(firstElementOfPreviousSite);
        StringBuilder builder = new StringBuilder();
        builder.append(basicPayloadFormFront)
                .append(formattedElement)
                .append(basicPayLoadFormBack);
        return builder.toString();
    }

    private String createProperElementForm(String firstElementOfPreviousSite) throws UnsupportedEncodingException {
        return URLEncoder.encode(firstElementOfPreviousSite, StandardCharsets.UTF_8.toString());
    }
}
