package com.nagygergely.dataminer.data;

import com.nagygergely.dataminer.webconnection.HTTPConnection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class LinkShuffler {

    public static List<String> generateLinks(Document document) {
        List<String> absoluteLinks = new ArrayList<>(); //Creating a list for the generated links.
        Elements linkElements = document.select("table.exp a");
        List<String> relativeLinks = linkElements.eachAttr("href");
        String rootURL = "http://193.224.93.3";
        for(String element : relativeLinks){
            String absoluteLink = rootURL + element;
            absoluteLinks.add(absoluteLink);
        }
        return absoluteLinks;
    }

    public static void main(String[] args) {
        try {
            HttpResponse<String> response = new HTTPConnection()
                    .makeConnection("xexp=B%F6ng%E9sz%E9s&index1=au&value1=&xperpage=20");
            Document document = Jsoup.parse(response.body());
            generateLinks(document);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //StringUtil.resolve -> creating az absolute link
}
