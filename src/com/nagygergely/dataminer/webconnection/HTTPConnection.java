package com.nagygergely.dataminer.webconnection;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HTTPConnection {
    private final String rootURL;

    public HTTPConnection() {
        this.rootURL = "http://193.224.93.3/cgi-bin/tlwww.cgi"; ;
    }

    public HttpResponse<String> makeConnection(String requestPayload) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .build();

        HttpRequest mainRequest = HttpRequest.newBuilder()
                .uri(URI.create(rootURL))
                .POST(HttpRequest.BodyPublishers.ofString(requestPayload))
                .build();

        return httpClient.send(mainRequest, HttpResponse.BodyHandlers.ofString());
    }
}
