package com.nagygergely.dataminer.webconnection;

import com.nagygergely.dataminer.data.RequestPayloadGenerator;
import com.nagygergely.dataminer.database.DataInserter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.List;

public class RequestMain {
    private static final HTTPConnection connection = new HTTPConnection();
    private static final RequestPayloadGenerator generator = new RequestPayloadGenerator();

    public static void main(String[] args) {
        try {
            String requestPayload = "xexp=B%F6ng%E9sz%E9s&index1=au&value1=&xperpage=20";
            HttpResponse<String> response = connection.makeConnection(requestPayload);
            mineNames(response);
        } catch (Exception e) {
            System.out.println("Error : " + e.getMessage());
        }
    }

    private static void addNamesToDB(List<String> authorNames) {
        try {
            for (String author : authorNames) {
                DataInserter.addDataToDatabase("authors", author);
            }
            System.out.println("Names had been added " + authorNames.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void mineNames(HttpResponse<String> response) throws IOException, InterruptedException {
        Document document = Jsoup.parse(response.body());
        Elements names = document.select("table.exp a[title='Keresés erre']");
        List<String> nameList = names.eachText();
        addNamesToDB(nameList);
        String firstElement = names.get(0).text();
        String requestPayload = generator.generatePayload(firstElement);
        HttpResponse<String> newResponse = connection.makeConnection(requestPayload);
        mineNames(newResponse);
        System.out.println("Recursion has stopped.");
    }
}
