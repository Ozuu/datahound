package com.nagygergely.dataminer.database;

import java.sql.*;

/**
 * DataInserter is a simple class for adding data to a particular table in your database.
 */

public class DataInserter {
    private static final String DB_URL = "jdbc:postgresql://localhost/libraryDatabase";
    private static final String USER = "system_user";
    private static final String PASS = "user123";
    private static final String SQL_PATTERN = """
            INSERT INTO %s ("%s")
            VALUES ('%s');""";

    public static void addDataToDatabase(String tableName, String element) throws SQLException, ClassNotFoundException {
        try (Connection connection = DriverManager.getConnection(DB_URL, USER, PASS)) {
            PreparedStatement pstmt = connection.prepareStatement("INSERT INTO authors (\"authorName\") VALUES (?);");
            pstmt.setString(1, element);
            Class.forName("org.postgresql.Driver");
            pstmt.execute();
        }
    }
}
